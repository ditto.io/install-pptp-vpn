# Install PPTP VPN

These scripts are designed to provide a rapid way to create a PPTP VPN on cloud hosting providers like [Vultr](http://vultr.com) and [Digital Ocean](https://www.digitalocean.com/). They can be used from the command line or from a startup script.

## Installs a PPTP on Debian 10

```bash
curl -sSL https://gitlab.com/ditto.io/install-pptp-vpn/raw/master/install-pptp-debian-10.sh | bash
echo "<username_1> pptpd <password_1> *" >> /etc/ppp/chap-secrets
echo "<username_2> pptpd <password_2> *" >> /etc/ppp/chap-secrets
```

## Installs a PPTP on Debian 9

```bash
curl -sSL https://gitlab.com/ditto.io/install-pptp-vpn/raw/master/install-pptp-debian-9.sh | bash
echo "<username_1> pptpd <password_1> *" >> /etc/ppp/chap-secrets
echo "<username_2> pptpd <password_2> *" >> /etc/ppp/chap-secrets
```

## Installs a PPTP on Debian 8

```bash
curl -sSL https://gitlab.com/ditto.io/install-pptp-vpn/raw/master/install-pptp-debian-8.sh | bash
echo "<username_1> pptpd <password_1> *" >> /etc/ppp/chap-secrets
echo "<username_2> pptpd <password_2> *" >> /etc/ppp/chap-secrets
```
