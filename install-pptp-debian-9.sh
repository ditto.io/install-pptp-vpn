#!/usr/bin/env bash

echo "####################################"
echo "# starting script"
echo "####################################"

apt update && apt -y dist-upgrade && apt -y --purge autoremove && apt clean

echo iptables-persistent iptables-persistent/autosave_v4 boolean true | debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | debconf-set-selections

apt -y install sudo vim screen tmux wget htop zip bzip2 ufw ntp openssh-server open-vm-tools pptpd iptables-persistent

touch /var/swap.img
chmod 600 /var/swap.img
dd if=/dev/zero of=/var/swap.img bs=1024k count=1000
mkswap /var/swap.img
swapon /var/swap.img
echo "/var/swap.img    none    swap    sw    0    0" >> /etc/fstab

systemctl enable pptpd

echo "localip 192.168.2.1" >> /etc/pptpd.conf
echo "remoteip 192.168.2.2-254" >> /etc/pptpd.conf
echo "ms-dns 8.8.8.8" >> /etc/ppp/pptpd-options
echo "ms-dns 8.8.4.4" >> /etc/ppp/pptpd-options
echo "noipx" >> /etc/ppp/pptpd-options

service pptpd restart

echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf

sysctl -p

interfaces=(`ls /sys/class/net`)
interface1=${interfaces[0]}

iptables -t nat -A POSTROUTING -o $interface1 -j MASQUERADE
iptables -I FORWARD -p tcp --tcp-flags SYN,RST SYN -s 192.168.2.0/24 -j TCPMSS --clamp-mss-to-pmtu
dpkg-reconfigure -u iptables-persistent

echo "####################################"
echo "# script complete"
echo "####################################"
