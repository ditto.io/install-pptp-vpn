#!/usr/bin/env bash

echo "####################################"
echo "# starting script"
echo "####################################"

apt-get update && apt-get -y dist-upgrade && apt-get -y --purge autoremove && apt-get clean

apt-get -y install sudo vim screen tmux wget htop zip bzip2 ufw ntp openssh-server open-vm-tools

sudo touch /var/swap.img
sudo chmod 600 /var/swap.img
sudo dd if=/dev/zero of=/var/swap.img bs=1024k count=1000
sudo mkswap /var/swap.img
sudo swapon /var/swap.img
sudo echo "/var/swap.img    none    swap    sw    0    0" >> /etc/fstab

sudo apt-get -y install pptpd

echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections

sudo apt-get -y install iptables-persistent
sudo systemctl enable pptpd

sudo echo "# IP address to assign to this server" >> /etc/pptpd.conf
sudo echo "localip 192.168.2.1" >> /etc/pptpd.conf
sudo echo "# IP address pool to assign clients" >> /etc/pptpd.conf
sudo echo "remoteip 192.168.2.2-254" >> /etc/pptpd.conf
sudo echo "ms-dns 8.8.8.8" >> /etc/ppp/pptpd-options
sudo echo "ms-dns 8.8.4.4" >> /etc/ppp/pptpd-options
sudo echo "noipx" >> /etc/ppp/pptpd-options

sudo service pptpd restart

sudo echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf

sudo sysctl -p

sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -I FORWARD -p tcp --tcp-flags SYN,RST SYN -s 192.168.2.0/24 -j TCPMSS --clamp-mss-to-pmtu
sudo dpkg-reconfigure iptables-persistent

echo "####################################"
echo "# script complete"
echo "####################################"